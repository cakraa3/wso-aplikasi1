<?php 
    include '../conf.php';
    session_start();
    unset($_SESSION["AUTH"]);
    header('Location: http://'. $APP_HOST_NAME .'/index.php');
?>