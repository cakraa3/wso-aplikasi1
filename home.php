<?php 
	include 'conf.php';

	session_start();
    if (empty($_SESSION["AUTH"])) {
		header('Location: http://'. $APP_HOST_NAME .'/index.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Aplikasi WSO | 1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
				<form class="login100-form validate-form flex-sb flex-w" action="http://10.100.38.49/wso-aplikasi1/actions/logout.php">
					<span class="login100-form-title p-b-15">
						Testing WSO IS
                    </span>
                    
                    <div class="p-t-31 p-b-9" style="width:100%; text-align:center">
						<span>
							You're logging!
						</span>
                    </div>
                    
					<div class="container-login100-form-btn m-t-17">
						<div class="login100-form-btn" onclick="logoutWSO()">
							Logout
                        </div>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="js/main.js"></script>
    
    <script>
        function logoutWSO() {

            $.get("http://<?=$APP_HOST_NAME?>/actions/callback_delete_session.php",
            {
            },
            function(data,status){
            });

            var LOGOUT_URL = 'https://<?=$IS_HOST_NAME?>:<?=$IS_PORT?>/oidc/logout?id_token_hint=<?php echo $_SESSION['AUTH']['token']?>&post_logout_redirect_uri=<?=$CALLBACK_EP?>&state=<?php echo $_SESSION['AUTH']['session_state']?>';
            window.location.href = LOGOUT_URL;
        }
    </script>
</body>
</html>